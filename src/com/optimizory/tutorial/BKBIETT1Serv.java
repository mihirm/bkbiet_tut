package com.optimizory.tutorial;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.optimizory.tutorial.beans.Student;
import com.optimizory.tutorial.dao.UserDao;

/**
 * Servlet implementation class BKBIETT1Serv
 */
public class BKBIETT1Serv extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BKBIETT1Serv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		StringBuffer stringBuff = new StringBuffer();
//		
//		stringBuff.append("<table><tr><th>Name</th><th>Stream</th><th>Email</th></tr>");
//		
		ResultSet rs = userDao.getData();
		
		ArrayList<Student> arr = null;
		
		try {
			arr = new ArrayList<Student>();
				while(rs.next()) {

					Student student = new Student();
					student.setName(rs.getString("name"));
					student.setStream(rs.getString("stream")) ;
					student.setEmail(rs.getString("email"));
					
					arr.add(student);
					rs.next();
					
				}
				

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.setAttribute("studentList", arr);
		request.getRequestDispatcher("/default.jsp").forward(request, response);
		
//		PrintWriter out = response.getWriter();
//
//		out.println("<html>");
//		out.println("<body>");
//		out.println("<h1>Hello World Servlet in BKBIET</h1>");
//		out.println(stringBuff.toString());
//		out.println("</body>");
//		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String stream = request.getParameter("stream");
		
		userDao.insertData(name, stream, email);
		
		doGet(request, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
		userDao.destroy();
	}

	@Override
	public void init() throws ServletException {

		super.init();
		
		userDao = new UserDao();
		userDao.init();
		
	}
	
	

}
