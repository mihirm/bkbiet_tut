package com.optimizory.tutorial.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
	private Connection connection = null;
	private String selectSQL = "SELECT * FROM bkbiet";
	private String insertTableSQL = "INSERT INTO bkbiet"
			+ "(name, stream, email) VALUES"
			+ "(?,?,?)";

	public void init() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			connection = DriverManager
			.getConnection("jdbc:mysql://localhost:3306/bkbiet","mihir", "mihir123");
			

	 
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
	}
	
	public boolean insertData(String name,String stream,String email) {
		if(connection != null) {
			
			PreparedStatement preparedStatement = null;
			
			try {
				
				preparedStatement = connection.prepareStatement(insertTableSQL);
				
				preparedStatement.setString(1, name);
				preparedStatement.setString(2, stream);
				preparedStatement.setString(3, email);
				
				preparedStatement.execute();

				
			}catch(SQLException sqle) {
				sqle.printStackTrace();
				return false;
			}
			return true;
		}
		return false;
	}
	
	public ResultSet getData() {
		
		if(connection != null) {
			System.out.println("You made it, take control your database now!");
			
			PreparedStatement preparedStatement = null;
			
			try {
								
				preparedStatement = connection.prepareStatement(selectSQL);
				
				ResultSet rs = preparedStatement.executeQuery();
				
//				while(rs.next()) {
//					String db_name = rs.getString("name");
//					String db_stream = rs.getString("stream");
//					String db_email = rs.getString("email");
//					System.out.println(db_name+ " "+ db_stream +" "+ db_email);
//					
//				}
				
				return rs;
				
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return null;
	}
	
	public void destroy() {
		if(connection != null){
			
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
